﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeuCurriculo.Models
{
    public class Query
    {
        public Query()
        {
            Page = 0;
            PageSize = 20;
        }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public int Total { get; set; }
        public bool HasNext
        {
            get
            {
                return (Page * PageSize) < (Total - PageSize);
            }
            set { }
        }
        public bool HasPrevious
        {
            get
            {
                return Page > 0;
            }
            set { }
        }
    }
}
