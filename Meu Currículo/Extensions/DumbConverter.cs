﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeuCurriculo.Extensions
{
    /// <summary>
    /// Retorna uma string para um valor booleano
    /// </summary>
    public static class DumbConverter
    {
        public static string ToDatabaseValue(this bool valor) => valor ? "sim" : null;
    }
}
