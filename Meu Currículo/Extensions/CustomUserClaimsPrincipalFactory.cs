﻿using MeuCurriculo.Data;
using MeuCurriculo.Data.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MeuCurriculo.Extensions
{
    public class CustomUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<User, Role>
    {
        private readonly ApplicationDbContext _applicationContext;

        public CustomUserClaimsPrincipalFactory(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IOptions<IdentityOptions> optionsAccessor,
            ApplicationDbContext applicationContext)
            : base(userManager, roleManager, optionsAccessor)
        {
            _applicationContext = applicationContext;
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(User user)
        {
            //var contatoEmpresa = await _applicationContext.Users.SingleAsync(s => s.Id == user.Id);
            ClaimsIdentity identity = await base.GenerateClaimsAsync(user);

            if (!string.IsNullOrEmpty(user.Name))
            {
                identity.AddClaim(new Claim("name", (user.Name.Contains(" ") ? user.Name.Split(" ")[0] : user.Name)));
            }
            else
            {
                identity.AddClaim(new Claim("name", "Usuário"));
            }

            return identity;
        }
    }
}
