﻿using Dapper;
using MeuCurriculo.Data;
using MeuCurriculo.Data.Domain;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeuCurriculo
{
    public class CurriculoService
    {
        private readonly IConfiguration _configuration;
        private readonly MeuCurriculoDbContext _meuCurriculoContext;

        //TODO get database from the connectionstring
        //private readonly string databaseName = "MeuCurriculoDB";

        public CurriculoService(
            IConfiguration configuration,
            MeuCurriculoDbContext meuCurriculoContext)
        {
            _configuration = configuration;
            _meuCurriculoContext = meuCurriculoContext;
        }

        //private string GetTable(string a, string b) => string.Format("`{0}`.`{1}`", a, b);
        public async Task<F5Curriculos> Get(int id)
        {
            return await _meuCurriculoContext
                .F5Curriculos
                .AsNoTracking()
                .SingleAsync(s => s.IdCurriculo == id);
            //using (var connection = new MySqlConnection(
            //    _configuration.GetConnectionString("DefaultConnection")))
            //{
            //    connection.Open();

            //    string selectSql = "SELECT * FROM "
            //        + GetTable(databaseName, "f5_curriculos")
            //        + "WHERE id_curriculo = "
            //        + id;

            //    var row = await connection.QueryAsync<F5Curriculos>(selectSql);

            //    return row.SingleOrDefault();
            //}
        }

        public async Task<IEnumerable<F5Curriculos>> GetAll()
        {
            return await _meuCurriculoContext
                .F5Curriculos
                .AsNoTracking()
                .ToListAsync();
            //TODO size
            //using (var connection = new MySqlConnection(
            //    _configuration.GetConnectionString("DefaultConnection")))
            //{
            //    connection.Open();

            //    string selectSql = "SELECT * FROM " +
            //        GetTable(databaseName, "f5_curriculos");

            //    var rows = await connection.QueryAsync<F5Curriculos>(selectSql);

            //    return rows;
            //}
        }

        public async Task<IEnumerable<F5CurriculosCampos>> GetCampos(int id)
        {
            return await _meuCurriculoContext
                .F5CurriculosCampos
                .AsNoTracking()
                .Where(s => s.IdCurriculo == id).ToListAsync() ?? new List<F5CurriculosCampos>();
            //using (var connection = new MySqlConnection(
            //    _configuration.GetConnectionString("DefaultConnection")))
            //{
            //    connection.Open();

            //    string selectSql = "SELECT * FROM "
            //        + GetTable(databaseName, "f5_curriculos_campos")
            //        + "WHERE id_curriculo = "
            //        + id;

            //    var rows = await connection.QueryAsync<F5CurriculosCampos>(selectSql);

            //    return rows ?? new List<F5CurriculosCampos>();
            //}
        }

        public async Task<IEnumerable<F5CurriculosOrdenacao>> GetOrdenacoes(int id)
        {
            return await _meuCurriculoContext
                .F5CurriculosOrdenacao
                .AsNoTracking()
                .Where(s => s.IdCurriculo == id).ToListAsync() ?? new List<F5CurriculosOrdenacao>();
            //using (var connection = new MySqlConnection(
            //    _configuration.GetConnectionString("DefaultConnection")))
            //{
            //    connection.Open();

            //    string selectSql = "SELECT * FROM "
            //        + GetTable(databaseName, "f5_curriculos_ordenacao")
            //        + "WHERE id_curriculo = "
            //        + id;

            //    var rows = await connection.QueryAsync<F5CurriculosOrdenacao>(selectSql);

            //    return rows ?? new List<F5CurriculosOrdenacao>();
            //}
        }

        public async Task<IEnumerable<F5CurriculosPersonalizacao>> GetPersonalizacoes(int id)
        {
            return await _meuCurriculoContext
                .F5CurriculosPersonalizacao
                .AsNoTracking()
                .Where(s => s.IdCurriculo == id).ToListAsync() ?? new List<F5CurriculosPersonalizacao>();
        }

        public async Task<IEnumerable<F5CurriculosVinculados>> GetVinculados(int id)
        {
            return await _meuCurriculoContext
                .F5CurriculosVinculados
                .AsNoTracking()
                .Where(s => s.IdCurriculoPrimario == id
                || s.IdCurriculoOutro == id).ToListAsync() ?? new List<F5CurriculosVinculados>();
        }

        //public async Task Insert(int id)
        //{
        //    using (var connection = new SqlConnection(
        //        _configuration.GetConnectionString("DefaultConnection")))
        //    {
        //        connection.Open();

        //        var cu = new F5Curriculos()
        //        {

        //        };
        //        string insertSql = "INSERT INTO " +
        //            GetTable(databaseName, schoolTableName) + " (" +
        //            "NAME, " +
        //            "DOCUMENT_TYPE, " +
        //            "DOCUMENT_NUMBER, " +
        //            "VALUES (" +
        //            "@Name, " +
        //            "@DocumentType, " +
        //            "@DocumentNumber)";
        //        int affectedRows = await connection.ExecuteAsync(insertSql, cu);

        //        var schId = connection.Query<int>(
        //            "SELECT id_curriculo FROM " +
        //            GetTable(databaseName, schoolTableName) +
        //            " WHERE id_curriculo = '" + documentNumber + "'").Single();

        //        return schId;
        //    }
        //}
    }
}
