﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeuCurriculo.Data
{
    public abstract class Entity
    {
        [Display(Name = "Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Display(Name = "Data de Criação")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Ultima Atualização")]
        public DateTime? UpdatedAt { get; set; }
    }
}
