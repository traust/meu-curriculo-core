﻿using System;
using MeuCurriculo.Data.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MeuCurriculo.Data
{
    public partial class MeuCurriculoDbContext : DbContext
    {
        public MeuCurriculoDbContext()
        {
        }

        public MeuCurriculoDbContext(DbContextOptions<MeuCurriculoDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<F5AuxSessoes> F5AuxSessoes { get; set; }
        public virtual DbSet<F5Clientes> F5Clientes { get; set; }
        public virtual DbSet<F5Cupons> F5Cupons { get; set; }
        public virtual DbSet<F5Curriculos> F5Curriculos { get; set; }
        public virtual DbSet<F5CurriculosCampos> F5CurriculosCampos { get; set; }
        public virtual DbSet<F5CurriculosOrdenacao> F5CurriculosOrdenacao { get; set; }
        public virtual DbSet<F5CurriculosPersonalizacao> F5CurriculosPersonalizacao { get; set; }
        public virtual DbSet<F5CurriculosVinculados> F5CurriculosVinculados { get; set; }
        public virtual DbSet<F5Depoimentos> F5Depoimentos { get; set; }
        public virtual DbSet<F5Depoimentos2> F5Depoimentos2 { get; set; }
        public virtual DbSet<F5EmailMarketing> F5EmailMarketing { get; set; }
        public virtual DbSet<F5Empresas> F5Empresas { get; set; }
        public virtual DbSet<F5EmpresasEmails> F5EmpresasEmails { get; set; }
        public virtual DbSet<F5EmpresasTelefones> F5EmpresasTelefones { get; set; }
        public virtual DbSet<F5Formularios> F5Formularios { get; set; }
        public virtual DbSet<F5Helps> F5Helps { get; set; }
        public virtual DbSet<F5Layouts> F5Layouts { get; set; }
        public virtual DbSet<F5LogAcessos> F5LogAcessos { get; set; }
        public virtual DbSet<F5Postagens> F5Postagens { get; set; }
        public virtual DbSet<F5PostagensImagens> F5PostagensImagens { get; set; }
        public virtual DbSet<F5SiteConfiguracoes> F5SiteConfiguracoes { get; set; }
        public virtual DbSet<F5SiteConfiguracoesHorarios> F5SiteConfiguracoesHorarios { get; set; }
        public virtual DbSet<F5SitePostagens> F5SitePostagens { get; set; }
        public virtual DbSet<F5SitePostagensImagens> F5SitePostagensImagens { get; set; }
        public virtual DbSet<F5Usuarios> F5Usuarios { get; set; }
        public virtual DbSet<F5UsuariosFuncoes> F5UsuariosFuncoes { get; set; }
        public virtual DbSet<F5UsuariosTelefones> F5UsuariosTelefones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging(true);

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=108.179.252.57;database=meucur63_bd;user=meucur63_bd;password=\"%mwuYn6=^tW#\";treattinyasboolean=true", x => x.ServerVersion("5.6.41-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<F5AuxSessoes>(entity =>
            {
                entity.HasKey(e => e.IdSessao)
                    .HasName("PRIMARY");

                entity.ToTable("f5_aux_sessoes");

                entity.Property(e => e.IdSessao)
                    .HasColumnName("id_sessao")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<F5Clientes>(entity =>
            {
                entity.HasKey(e => e.IdCliente)
                    .HasName("PRIMARY");

                entity.ToTable("f5_clientes");

                entity.Property(e => e.IdCliente)
                    .HasColumnName("id_cliente")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ativo)
                    .IsRequired()
                    .HasColumnName("ativo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Bairro)
                    .IsRequired()
                    .HasColumnName("bairro")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Celular)
                    .IsRequired()
                    .HasColumnName("celular")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cep)
                    .IsRequired()
                    .HasColumnName("cep")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cidade)
                    .IsRequired()
                    .HasColumnName("cidade")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Complemento)
                    .IsRequired()
                    .HasColumnName("complemento")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cpf)
                    .IsRequired()
                    .HasColumnName("cpf")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Endereco)
                    .IsRequired()
                    .HasColumnName("endereco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Nascimento)
                    .HasColumnName("nascimento")
                    .HasColumnType("date");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Observacoes)
                    .IsRequired()
                    .HasColumnName("observacoes")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Senha)
                    .IsRequired()
                    .HasColumnName("senha")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Telefone)
                    .IsRequired()
                    .HasColumnName("telefone")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5Cupons>(entity =>
            {
                entity.HasKey(e => e.IdCupom)
                    .HasName("PRIMARY");

                entity.ToTable("f5_cupons");

                entity.Property(e => e.IdCupom)
                    .HasColumnName("id_cupom")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasColumnName("codigo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.DataFinal)
                    .HasColumnName("data_final")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<F5Curriculos>(entity =>
            {
                entity.HasKey(e => e.IdCurriculo)
                    .HasName("PRIMARY");

                entity.ToTable("f5_curriculos");

                entity.HasIndex(e => e.DataCadastro)
                    .HasName("id_pagseguro");

                entity.Property(e => e.IdCurriculo)
                    .HasColumnName("id_curriculo")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Bairro)
                    .IsRequired()
                    .HasColumnName("bairro")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Celular)
                    .IsRequired()
                    .HasColumnName("celular")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cep)
                    .IsRequired()
                    .HasColumnName("cep")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cidade)
                    .IsRequired()
                    .HasColumnName("cidade")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cpf)
                    .IsRequired()
                    .HasColumnName("cpf")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cupom)
                    .IsRequired()
                    .HasColumnName("cupom")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CupomTipo)
                    .IsRequired()
                    .HasColumnName("cupom_tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CupomValor)
                    .HasColumnName("cupom_valor")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.DataExpiracao)
                    .HasColumnName("data_expiracao")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Endereco)
                    .IsRequired()
                    .HasColumnName("endereco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.EstadoCivil)
                    .IsRequired()
                    .HasColumnName("estado_civil")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Expirou)
                    .IsRequired()
                    .HasColumnName("expirou")
                    .HasColumnType("char(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FormaPagamento)
                    .IsRequired()
                    .HasColumnName("forma_pagamento")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FotoDestaque)
                    .IsRequired()
                    .HasColumnName("foto_destaque")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.GatewayPagamento)
                    .IsRequired()
                    .HasColumnName("gateway_pagamento")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdCliente)
                    .HasColumnName("id_cliente")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLayout)
                    .HasColumnName("id_layout")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdPlano)
                    .HasColumnName("id_plano")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Intercambios)
                    .IsRequired()
                    .HasColumnName("intercambios")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Interesses)
                    .IsRequired()
                    .HasColumnName("interesses")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.MostrarSite)
                    .IsRequired()
                    .HasColumnName("mostrar_site")
                    .HasColumnType("char(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nascimento)
                    .HasColumnName("nascimento")
                    .HasColumnType("date");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.ObservacoesInternas)
                    .IsRequired()
                    .HasColumnName("observacoes_internas")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.OutrasInformacoes)
                    .IsRequired()
                    .HasColumnName("outras_informacoes")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Pais)
                    .IsRequired()
                    .HasColumnName("pais")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Perfil)
                    .IsRequired()
                    .HasColumnName("perfil")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.PretensaoSalarial)
                    .IsRequired()
                    .HasColumnName("pretensao_salarial")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Referencias)
                    .IsRequired()
                    .HasColumnName("referencias")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasColumnName("sexo")
                    .HasColumnType("char(1)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Site)
                    .IsRequired()
                    .HasColumnName("site")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Situacao)
                    .IsRequired()
                    .HasColumnName("situacao")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Telefone)
                    .IsRequired()
                    .HasColumnName("telefone")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Telefone2)
                    .IsRequired()
                    .HasColumnName("telefone2")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Voluntariado)
                    .IsRequired()
                    .HasColumnName("voluntariado")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

            });

            modelBuilder.Entity<F5CurriculosCampos>(entity =>
            {
                entity.ToTable("f5_curriculos_campos");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Campo1)
                    .IsRequired()
                    .HasColumnName("campo_1")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Campo2)
                    .IsRequired()
                    .HasColumnName("campo_2")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Cidade)
                    .IsRequired()
                    .HasColumnName("cidade")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdCurriculo)
                    .HasColumnName("id_curriculo")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Inicio)
                    .IsRequired()
                    .HasColumnName("inicio")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Termino)
                    .IsRequired()
                    .HasColumnName("termino")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<F5CurriculosOrdenacao>(entity =>
            {
                entity.HasKey(e => e.IdOrdenacao)
                    .HasName("PRIMARY");

                entity.ToTable("f5_curriculos_ordenacao");

                entity.Property(e => e.IdOrdenacao)
                    .HasColumnName("id_ordenacao")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCurriculo)
                    .HasColumnName("id_curriculo")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Posicao)
                    .HasColumnName("posicao")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<F5CurriculosPersonalizacao>(entity =>
            {
                entity.HasKey(e => e.IdPersonalizacao)
                    .HasName("PRIMARY");

                entity.ToTable("f5_curriculos_personalizacao");

                entity.Property(e => e.IdPersonalizacao)
                    .HasColumnName("id_personalizacao")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.CorFundo)
                    .IsRequired()
                    .HasColumnName("cor_fundo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorPrincipal)
                    .IsRequired()
                    .HasColumnName("cor_principal")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorSubtitulos)
                    .IsRequired()
                    .HasColumnName("cor_subtitulos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorTextos)
                    .IsRequired()
                    .HasColumnName("cor_textos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorTitulos)
                    .IsRequired()
                    .HasColumnName("cor_titulos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.FonteTextos)
                    .IsRequired()
                    .HasColumnName("fonte_textos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FonteTitulos)
                    .IsRequired()
                    .HasColumnName("fonte_titulos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdCurriculo)
                    .HasColumnName("id_curriculo")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<F5CurriculosVinculados>(entity =>
            {
                entity.HasKey(e => e.IdVinculado)
                    .HasName("PRIMARY");

                entity.ToTable("f5_curriculos_vinculados");

                entity.Property(e => e.IdVinculado)
                    .HasColumnName("id_vinculado")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCurriculoOutro)
                    .HasColumnName("id_curriculo_outro")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdCurriculoPrimario)
                    .HasColumnName("id_curriculo_primario")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<F5Depoimentos>(entity =>
            {
                entity.HasKey(e => e.IdDepoimento)
                    .HasName("PRIMARY");

                entity.ToTable("f5_depoimentos");

                entity.Property(e => e.IdDepoimento)
                    .HasColumnName("id_depoimento")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Depoimento)
                    .IsRequired()
                    .HasColumnName("depoimento")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Endereco)
                    .IsRequired()
                    .HasColumnName("endereco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FotoDestaque)
                    .IsRequired()
                    .HasColumnName("foto_destaque")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<F5Depoimentos2>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("f5_depoimentos2");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasColumnType("varchar(400)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.FotoDestaque)
                    .IsRequired()
                    .HasColumnName("foto_destaque")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdDepoimento)
                    .HasColumnName("id_depoimento")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<F5EmailMarketing>(entity =>
            {
                entity.HasKey(e => e.IdEmailMarketing)
                    .HasName("PRIMARY");

                entity.ToTable("f5_email_marketing");

                entity.Property(e => e.IdEmailMarketing)
                    .HasColumnName("id_email_marketing")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Mensagem)
                    .HasColumnName("mensagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Titulo)
                    .HasColumnName("titulo")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<F5Empresas>(entity =>
            {
                entity.HasKey(e => e.IdEmpresa)
                    .HasName("PRIMARY");

                entity.ToTable("f5_empresas");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("id_empresa")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AceitouTermos)
                    .IsRequired()
                    .HasColumnName("aceitou_termos")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Ativo)
                    .IsRequired()
                    .HasColumnName("ativo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Bairro)
                    .IsRequired()
                    .HasColumnName("bairro")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cep)
                    .HasColumnName("cep")
                    .HasColumnType("char(9)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cidade)
                    .HasColumnName("cidade")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cnpj)
                    .IsRequired()
                    .HasColumnName("cnpj")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Complemento)
                    .IsRequired()
                    .HasColumnName("complemento")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Endereco)
                    .HasColumnName("endereco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Facebook)
                    .IsRequired()
                    .HasColumnName("facebook")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Logotipo)
                    .IsRequired()
                    .HasColumnName("logotipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.NomeFantasia)
                    .IsRequired()
                    .HasColumnName("nome_fantasia")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Numero)
                    .HasColumnName("numero")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Observacoes)
                    .IsRequired()
                    .HasColumnName("observacoes")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.RazaoSocial)
                    .IsRequired()
                    .HasColumnName("razao_social")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Site)
                    .IsRequired()
                    .HasColumnName("site")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5EmpresasEmails>(entity =>
            {
                entity.HasKey(e => e.IdEmail)
                    .HasName("PRIMARY");

                entity.ToTable("f5_empresas_emails");

                entity.Property(e => e.IdEmail)
                    .HasColumnName("id_email")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_bin");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("id_empresa")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<F5EmpresasTelefones>(entity =>
            {
                entity.HasKey(e => e.IdTelefone)
                    .HasName("PRIMARY");

                entity.ToTable("f5_empresas_telefones");

                entity.Property(e => e.IdTelefone)
                    .HasColumnName("id_telefone")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("id_empresa")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Operadora)
                    .IsRequired()
                    .HasColumnName("operadora")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5Formularios>(entity =>
            {
                entity.HasKey(e => e.IdFormulario)
                    .HasName("PRIMARY");

                entity.ToTable("f5_formularios");

                entity.Property(e => e.IdFormulario)
                    .HasColumnName("id_formulario")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Mensagem)
                    .IsRequired()
                    .HasColumnName("mensagem")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Telefone)
                    .IsRequired()
                    .HasColumnName("telefone")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<F5Helps>(entity =>
            {
                entity.HasKey(e => e.IdHelp)
                    .HasName("PRIMARY");

                entity.ToTable("f5_helps");

                entity.Property(e => e.IdHelp)
                    .HasColumnName("id_help")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Bloco)
                    .IsRequired()
                    .HasColumnName("bloco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Campo)
                    .IsRequired()
                    .HasColumnName("campo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo1)
                    .IsRequired()
                    .HasColumnName("exemplo_1")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo10)
                    .IsRequired()
                    .HasColumnName("exemplo_10")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo2)
                    .IsRequired()
                    .HasColumnName("exemplo_2")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo3)
                    .IsRequired()
                    .HasColumnName("exemplo_3")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo4)
                    .IsRequired()
                    .HasColumnName("exemplo_4")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo5)
                    .IsRequired()
                    .HasColumnName("exemplo_5")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo6)
                    .IsRequired()
                    .HasColumnName("exemplo_6")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo7)
                    .IsRequired()
                    .HasColumnName("exemplo_7")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo8)
                    .IsRequired()
                    .HasColumnName("exemplo_8")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplo9)
                    .IsRequired()
                    .HasColumnName("exemplo_9")
                    .HasColumnType("text")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Exemplos)
                    .IsRequired()
                    .HasColumnName("exemplos")
                    .HasColumnType("char(3)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.IdSessao)
                    .HasColumnName("id_sessao")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("titulo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<F5Layouts>(entity =>
            {
                entity.HasKey(e => e.IdLayout)
                    .HasName("PRIMARY");

                entity.ToTable("f5_layouts");

                entity.Property(e => e.IdLayout)
                    .HasColumnName("id_layout")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.CorFundo)
                    .IsRequired()
                    .HasColumnName("cor_fundo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorPrincipal)
                    .IsRequired()
                    .HasColumnName("cor_principal")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorSubtitulos)
                    .IsRequired()
                    .HasColumnName("cor_subtitulos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorTextos)
                    .IsRequired()
                    .HasColumnName("cor_textos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.CorTitulos)
                    .IsRequired()
                    .HasColumnName("cor_titulos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<F5LogAcessos>(entity =>
            {
                entity.HasKey(e => e.IdLog)
                    .HasName("PRIMARY");

                entity.ToTable("f5_log_acessos");

                entity.Property(e => e.IdLog)
                    .HasColumnName("id_log")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DataAcesso)
                    .HasColumnName("data_acesso")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdEmpresa)
                    .HasColumnName("id_empresa")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("id_usuario")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasColumnName("ip")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5Postagens>(entity =>
            {
                entity.HasKey(e => e.IdPostagem)
                    .HasName("PRIMARY");

                entity.ToTable("f5_postagens");

                entity.Property(e => e.IdPostagem)
                    .HasColumnName("id_postagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ativo)
                    .IsRequired()
                    .HasColumnName("ativo")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Autor)
                    .IsRequired()
                    .HasColumnName("autor")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Creditos)
                    .IsRequired()
                    .HasColumnName("creditos")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.DataPostagem)
                    .HasColumnName("data_postagem")
                    .HasColumnType("datetime");

                entity.Property(e => e.Destaque)
                    .IsRequired()
                    .HasColumnName("destaque")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FotoDestaque)
                    .IsRequired()
                    .HasColumnName("foto_destaque")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Keywords)
                    .IsRequired()
                    .HasColumnName("keywords")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Legenda)
                    .IsRequired()
                    .HasColumnName("legenda")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnName("link")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Resumo)
                    .IsRequired()
                    .HasColumnName("resumo")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Subtitulo)
                    .IsRequired()
                    .HasColumnName("subtitulo")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Texto)
                    .IsRequired()
                    .HasColumnName("texto")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("titulo")
                    .HasColumnType("varchar(500)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5PostagensImagens>(entity =>
            {
                entity.HasKey(e => e.IdImagem)
                    .HasName("PRIMARY");

                entity.ToTable("f5_postagens_imagens");

                entity.Property(e => e.IdImagem)
                    .HasColumnName("id_imagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Arquivo)
                    .IsRequired()
                    .HasColumnName("arquivo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPostagem)
                    .HasColumnName("id_postagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Legenda)
                    .IsRequired()
                    .HasColumnName("legenda")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnName("link")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Posicao)
                    .IsRequired()
                    .HasColumnName("posicao")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5SiteConfiguracoes>(entity =>
            {
                entity.HasKey(e => e.IdConfiguracao)
                    .HasName("PRIMARY");

                entity.ToTable("f5_site_configuracoes");

                entity.Property(e => e.IdConfiguracao)
                    .HasColumnName("id_configuracao")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Bairro)
                    .IsRequired()
                    .HasColumnName("bairro")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cep)
                    .IsRequired()
                    .HasColumnName("cep")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cidade)
                    .IsRequired()
                    .HasColumnName("cidade")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Cnpj)
                    .IsRequired()
                    .HasColumnName("cnpj")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Complemento)
                    .IsRequired()
                    .HasColumnName("complemento")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Endereco)
                    .IsRequired()
                    .HasColumnName("endereco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Facebook)
                    .IsRequired()
                    .HasColumnName("facebook")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.HorarioAtendimento)
                    .IsRequired()
                    .HasColumnName("horario_atendimento")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Instagram)
                    .IsRequired()
                    .HasColumnName("instagram")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Logotipo)
                    .IsRequired()
                    .HasColumnName("logotipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.MetaDescription)
                    .IsRequired()
                    .HasColumnName("meta_description")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.MetaKeywords)
                    .IsRequired()
                    .HasColumnName("meta_keywords")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.MetaTitle)
                    .IsRequired()
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.RazaoSocial)
                    .IsRequired()
                    .HasColumnName("razao_social")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Telefone)
                    .IsRequired()
                    .HasColumnName("telefone")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Telefone2)
                    .IsRequired()
                    .HasColumnName("telefone2")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Twitter)
                    .IsRequired()
                    .HasColumnName("twitter")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Whatsapp)
                    .IsRequired()
                    .HasColumnName("whatsapp")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5SiteConfiguracoesHorarios>(entity =>
            {
                entity.HasKey(e => e.IdHorario)
                    .HasName("PRIMARY");

                entity.ToTable("f5_site_configuracoes_horarios");

                entity.Property(e => e.IdHorario)
                    .HasColumnName("id_horario")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Horario)
                    .IsRequired()
                    .HasColumnName("horario")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5SitePostagens>(entity =>
            {
                entity.HasKey(e => e.IdPostagem)
                    .HasName("PRIMARY");

                entity.ToTable("f5_site_postagens");

                entity.Property(e => e.IdPostagem)
                    .HasColumnName("id_postagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.DataPostagem)
                    .HasColumnName("data_postagem")
                    .HasColumnType("date");

                entity.Property(e => e.Destaque)
                    .IsRequired()
                    .HasColumnName("destaque")
                    .HasColumnType("char(3)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Formato)
                    .IsRequired()
                    .HasColumnName("formato")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.FotoDestaque)
                    .IsRequired()
                    .HasColumnName("foto_destaque")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdCategoria)
                    .HasColumnName("id_categoria")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MetaDescription)
                    .IsRequired()
                    .HasColumnName("meta_description")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.MetaKeywords)
                    .IsRequired()
                    .HasColumnName("meta_keywords")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.MetaTitle)
                    .IsRequired()
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Resumo)
                    .IsRequired()
                    .HasColumnName("resumo")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Texto)
                    .IsRequired()
                    .HasColumnName("texto")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("titulo")
                    .HasColumnType("varchar(500)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.TituloHome)
                    .IsRequired()
                    .HasColumnName("titulo_home")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Vinculo)
                    .IsRequired()
                    .HasColumnName("vinculo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5SitePostagensImagens>(entity =>
            {
                entity.HasKey(e => e.IdImagem)
                    .HasName("PRIMARY");

                entity.ToTable("f5_site_postagens_imagens");

                entity.Property(e => e.IdImagem)
                    .HasColumnName("id_imagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Arquivo)
                    .IsRequired()
                    .HasColumnName("arquivo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("descricao")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.IdPostagem)
                    .HasColumnName("id_postagem")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnName("link")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Posicao)
                    .IsRequired()
                    .HasColumnName("posicao")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            modelBuilder.Entity<F5Usuarios>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PRIMARY");

                entity.ToTable("f5_usuarios");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("id_usuario")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ativo)
                    .IsRequired()
                    .HasColumnName("ativo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Bairro)
                    .IsRequired()
                    .HasColumnName("bairro")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Cep)
                    .IsRequired()
                    .HasColumnName("cep")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Cidade)
                    .IsRequired()
                    .HasColumnName("cidade")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Complemento)
                    .IsRequired()
                    .HasColumnName("complemento")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Endereco)
                    .IsRequired()
                    .HasColumnName("endereco")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnName("estado")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasColumnName("foto")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Nascimento)
                    .HasColumnName("nascimento")
                    .HasColumnType("date");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Senha)
                    .IsRequired()
                    .HasColumnName("senha")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasColumnName("sexo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");
            });

            modelBuilder.Entity<F5UsuariosFuncoes>(entity =>
            {
                entity.ToTable("f5_usuarios_funcoes");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFuncao)
                    .HasColumnName("id_funcao")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("id_usuario")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<F5UsuariosTelefones>(entity =>
            {
                entity.HasKey(e => e.IdTelefone)
                    .HasName("PRIMARY");

                entity.ToTable("f5_usuarios_telefones");

                entity.Property(e => e.IdTelefone)
                    .HasColumnName("id_telefone")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Atualizacao)
                    .HasColumnName("atualizacao")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DataCadastro)
                    .HasColumnName("data_cadastro")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("id_usuario")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Operadora)
                    .IsRequired()
                    .HasColumnName("operadora")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnName("tipo")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    }
}
