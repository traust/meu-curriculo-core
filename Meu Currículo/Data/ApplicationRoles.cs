﻿namespace MeuCurriculo.Data
{
    internal class ApplicationRoles
    {
        public static string Admin { get; internal set; } = "Admin";
        public static string Cliente { get; internal set; } = "Client";
    }
}