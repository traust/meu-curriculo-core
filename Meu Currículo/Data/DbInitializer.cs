﻿using System;
using System.Threading.Tasks;
using MeuCurriculo.Data.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace MeuCurriculo.Data
{
    internal class DbInitializer
    {
        internal static async Task Initialize(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            await AddRoles(roleManager);
            await AddUsers(userManager);
        }
        private static async Task AddRoles(RoleManager<Role> roleManager)
        {
            if (!await roleManager.RoleExistsAsync(ApplicationRoles.Admin))
                await roleManager.CreateAsync(new Role(ApplicationRoles.Admin));
            if (!await roleManager.RoleExistsAsync(ApplicationRoles.Cliente))
                await roleManager.CreateAsync(new Role(ApplicationRoles.Cliente));
        }

        private static async Task AddUsers(UserManager<User> userManager)
        {
            var user = await userManager.FindByEmailAsync("contato@traust.it");
            if (user == null)
            {
                user = new User
                {
                    Name = "Admin",
                    UserName = "contato@traust.it",
                    Email = "contato@traust.it",
                    CreatedAt = DateTime.Now
                };

                var result = await userManager.CreateAsync(user, "Qw121314!");
                if (result.Succeeded)
                {
                    string[] role = new string[] { ApplicationRoles.Admin };
                    await userManager.AddToRolesAsync(user, role);
                }
            }

            user = await userManager.FindByEmailAsync("elianagaspary@yahoo.com.br");
            if (user == null)
            {
                user = new User
                {
                    Name = "Eliana",
                    UserName = "elianagaspary@yahoo.com.br",
                    Email = "elianagaspary@yahoo.com.br",
                    CreatedAt = DateTime.Now
                };

                var res = await userManager.CreateAsync(user, "Pass@word1");
                if (res.Succeeded)
                {
                    string[] role = new string[] { ApplicationRoles.Admin };
                    await userManager.AddToRolesAsync(user, role);
                }
            }
        }
    }
}