﻿using Microsoft.AspNetCore.Identity;

namespace MeuCurriculo.Data.Identity
{
    public class UserRole : IdentityUserRole<int>
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
