﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Identity
{
    public class Role : IdentityRole<int>
    {
        public Role() { }

        public Role(string roleName) : base(roleName) { }
        public string Descricao { get; set; }
        public ICollection<UserRole> Users { get; set; }
    }
}
