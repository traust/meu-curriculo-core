﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5SitePostagensImagens
    {
        public int IdImagem { get; set; }
        public int IdPostagem { get; set; }
        public string Arquivo { get; set; }
        public string Posicao { get; set; }
        public string Link { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
