﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5CurriculosCampos
    {
        public int Id { get; set; }
        public int IdCurriculo { get; set; }
        public string Tipo { get; set; }
        public string Campo1 { get; set; }
        public string Campo2 { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Inicio { get; set; }
        public string Termino { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }

        public F5CurriculosCampos Clone()
        {
            return (F5CurriculosCampos)MemberwiseClone();
        }
    }
}
