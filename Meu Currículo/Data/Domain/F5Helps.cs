﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Helps
    {
        public int IdHelp { get; set; }
        public int IdSessao { get; set; }
        public string Bloco { get; set; }
        public string Campo { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Exemplos { get; set; }
        public string Exemplo1 { get; set; }
        public string Exemplo2 { get; set; }
        public string Exemplo3 { get; set; }
        public string Exemplo4 { get; set; }
        public string Exemplo5 { get; set; }
        public string Exemplo6 { get; set; }
        public string Exemplo7 { get; set; }
        public string Exemplo8 { get; set; }
        public string Exemplo9 { get; set; }
        public string Exemplo10 { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
