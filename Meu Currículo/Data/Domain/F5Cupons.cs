﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Cupons
    {
        public int IdCupom { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string Tipo { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataFinal { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
