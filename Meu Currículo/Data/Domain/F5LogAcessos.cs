﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5LogAcessos
    {
        public int IdLog { get; set; }
        public int IdEmpresa { get; set; }
        public int IdUsuario { get; set; }
        public string Tipo { get; set; }
        public string Ip { get; set; }
        public DateTime DataAcesso { get; set; }
    }
}
