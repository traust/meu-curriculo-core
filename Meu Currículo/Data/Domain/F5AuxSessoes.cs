﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5AuxSessoes
    {
        public int IdSessao { get; set; }
        public string Nome { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
