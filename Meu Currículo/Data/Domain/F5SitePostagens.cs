﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5SitePostagens
    {
        public int IdPostagem { get; set; }
        public int IdCategoria { get; set; }
        public string Vinculo { get; set; }
        public string Destaque { get; set; }
        public string Formato { get; set; }
        public string Tipo { get; set; }
        public string Titulo { get; set; }
        public string TituloHome { get; set; }
        public string Resumo { get; set; }
        public string Texto { get; set; }
        public string FotoDestaque { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public DateTime DataPostagem { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
