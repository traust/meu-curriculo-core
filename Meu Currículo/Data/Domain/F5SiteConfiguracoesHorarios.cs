﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5SiteConfiguracoesHorarios
    {
        public int IdHorario { get; set; }
        public string Descricao { get; set; }
        public string Horario { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
