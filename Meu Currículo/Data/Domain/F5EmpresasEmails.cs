﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5EmpresasEmails
    {
        public int IdEmail { get; set; }
        public int IdEmpresa { get; set; }
        public string Email { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
