﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5CurriculosPersonalizacao
    {
        public int IdPersonalizacao { get; set; }
        public int IdCurriculo { get; set; }
        public string FonteTitulos { get; set; }
        public string FonteTextos { get; set; }
        public string CorPrincipal { get; set; }
        public string CorFundo { get; set; }
        public string CorTitulos { get; set; }
        public string CorSubtitulos { get; set; }
        public string CorTextos { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }

        internal F5CurriculosPersonalizacao Clone()
        {
            return (F5CurriculosPersonalizacao)MemberwiseClone();
        }
    }
}
