﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5CurriculosVinculados
    {
        public int IdVinculado { get; set; }
        public int IdCurriculoPrimario { get; set; }
        public int IdCurriculoOutro { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }

        internal F5CurriculosVinculados Clone()
        {
            return (F5CurriculosVinculados)MemberwiseClone();
        }
    }
}
