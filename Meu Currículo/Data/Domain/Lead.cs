﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeuCurriculo.Data.Domain
{
    public class Lead : Entity
    {
        public string Nome { get; set; }
        public string Email { get; set; }
    }
}
