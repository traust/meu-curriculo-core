﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5UsuariosTelefones
    {
        public int IdTelefone { get; set; }
        public int IdUsuario { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public string Operadora { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
