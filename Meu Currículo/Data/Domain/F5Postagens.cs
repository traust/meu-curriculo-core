﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Postagens
    {
        public int IdPostagem { get; set; }
        public string Ativo { get; set; }
        public string Destaque { get; set; }
        public string Autor { get; set; }
        public string Tipo { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Resumo { get; set; }
        public string Texto { get; set; }
        public string FotoDestaque { get; set; }
        public string Creditos { get; set; }
        public string Legenda { get; set; }
        public string Link { get; set; }
        public string Keywords { get; set; }
        public DateTime DataPostagem { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
