﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Usuarios
    {
        public int IdUsuario { get; set; }
        public string Ativo { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public DateTime Nascimento { get; set; }
        public string Sexo { get; set; }
        public string Foto { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public DateTime Atualizacao { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}
