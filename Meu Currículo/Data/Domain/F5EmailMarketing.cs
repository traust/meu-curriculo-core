﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5EmailMarketing
    {
        public int IdEmailMarketing { get; set; }
        public int Titulo { get; set; }
        public int Mensagem { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
