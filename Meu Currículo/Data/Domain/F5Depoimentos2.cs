﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Depoimentos2
    {
        public int IdDepoimento { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string FotoDestaque { get; set; }
        public string Estado { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
