﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Empresas
    {
        public int IdEmpresa { get; set; }
        public string Ativo { get; set; }
        public string Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Site { get; set; }
        public string Facebook { get; set; }
        public string Logotipo { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Observacoes { get; set; }
        public string AceitouTermos { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
