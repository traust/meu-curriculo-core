﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5CurriculosOrdenacao
    {
        public int IdOrdenacao { get; set; }
        public int IdCurriculo { get; set; }
        public string Nome { get; set; }
        public int Posicao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }

        internal F5CurriculosOrdenacao Clone()
        {
            return (F5CurriculosOrdenacao)MemberwiseClone();
        }
    }
}
