﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Depoimentos
    {
        public int IdDepoimento { get; set; }
        public string Nome { get; set; }
        public string Depoimento { get; set; }
        public string FotoDestaque { get; set; }
        public string Endereco { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
