﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Layouts
    {
        public int IdLayout { get; set; }
        public string CorPrincipal { get; set; }
        public string CorFundo { get; set; }
        public string CorTitulos { get; set; }
        public string CorSubtitulos { get; set; }
        public string CorTextos { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
