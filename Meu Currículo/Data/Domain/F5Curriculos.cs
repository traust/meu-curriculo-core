﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5Curriculos
    {
        public int IdCurriculo { get; set; }
        public int IdCliente { get; set; }
        public int IdPlano { get; set; }
        public int IdLayout { get; set; }
        public string Sexo { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Telefone2 { get; set; }
        public string Celular { get; set; }
        public string Cpf { get; set; }
        public string FotoDestaque { get; set; }
        public DateTime Nascimento { get; set; }
        public string EstadoCivil { get; set; }
        public string Site { get; set; }
        public string MostrarSite { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public string Perfil { get; set; }
        public string Voluntariado { get; set; }
        public string Intercambios { get; set; }
        public string Interesses { get; set; }
        public string Referencias { get; set; }
        public string PretensaoSalarial { get; set; }
        public string OutrasInformacoes { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataExpiracao { get; set; }
        public decimal Valor { get; set; }
        public string Cupom { get; set; }
        public decimal CupomValor { get; set; }
        public string CupomTipo { get; set; }
        public string Situacao { get; set; }
        public string Expirou { get; set; }
        public string GatewayPagamento { get; set; }
        public string FormaPagamento { get; set; }
        public string ObservacoesInternas { get; set; }
        public DateTime Atualizacao { get; set; }

        internal F5Curriculos Clone()
        {
            return (F5Curriculos)MemberwiseClone();
        }
    }
}
