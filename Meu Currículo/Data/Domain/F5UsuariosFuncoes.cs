﻿using System;
using System.Collections.Generic;

namespace MeuCurriculo.Data.Domain
{
    public partial class F5UsuariosFuncoes
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdFuncao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime Atualizacao { get; set; }
    }
}
