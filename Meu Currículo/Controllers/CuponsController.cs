﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MeuCurriculo.Data;
using MeuCurriculo.Data.Domain;
using Microsoft.AspNetCore.Authorization;

namespace MeuCurriculo.Controllers
{
    [Authorize]
    public class CuponsController : Controller
    {
        private readonly MeuCurriculoDbContext _context;

        public CuponsController(MeuCurriculoDbContext context)
        {
            _context = context;
        }

        // GET: Cupons
        public async Task<IActionResult> Index(Models.Query query)
        {
            var cupons = await _context.F5Cupons
                .Skip(query.PageSize * query.Page)
                .Take(query.PageSize)
                .ToListAsync();

            query.Total = _context.F5Cupons.Count();
            ViewData["query"] = query;
            return View(cupons);
        }

        // GET: Cupons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Cupons = await _context.F5Cupons
                .FirstOrDefaultAsync(m => m.IdCupom == id);
            if (f5Cupons == null)
            {
                return NotFound();
            }

            return View(f5Cupons);
        }

        // GET: Cupons/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cupons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCupom,Nome,Codigo,Tipo,Valor,DataFinal,DataCadastro,Atualizacao")] F5Cupons f5Cupons)
        {
            if (ModelState.IsValid)
            {
                _context.Add(f5Cupons);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(f5Cupons);
        }

        // GET: Cupons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Cupons = await _context.F5Cupons.FindAsync(id);
            if (f5Cupons == null)
            {
                return NotFound();
            }
            return View(f5Cupons);
        }

        // POST: Cupons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCupom,Nome,Codigo,Tipo,Valor,DataFinal,DataCadastro,Atualizacao")] F5Cupons f5Cupons)
        {
            if (id != f5Cupons.IdCupom)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(f5Cupons);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!F5CuponsExists(f5Cupons.IdCupom))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(f5Cupons);
        }

        // GET: Cupons/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Cupons = await _context.F5Cupons
                .FirstOrDefaultAsync(m => m.IdCupom == id);
            if (f5Cupons == null)
            {
                return NotFound();
            }

            return View(f5Cupons);
        }

        // POST: Cupons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var f5Cupons = await _context.F5Cupons.FindAsync(id);
            _context.F5Cupons.Remove(f5Cupons);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool F5CuponsExists(int id)
        {
            return _context.F5Cupons.Any(e => e.IdCupom == id);
        }
    }
}
