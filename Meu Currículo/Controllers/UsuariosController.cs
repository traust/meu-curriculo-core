﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MeuCurriculo.Data;
using MeuCurriculo.Data.Domain;
using Microsoft.AspNetCore.Authorization;

namespace MeuCurriculo.Controllers
{
    [Authorize]
    public class UsuariosController : Controller
    {
        private readonly MeuCurriculoDbContext _context;

        public UsuariosController(MeuCurriculoDbContext context)
        {
            _context = context;
        }

        // GET: Usuarios
        public async Task<IActionResult> Index()
        {
            return View(await _context.F5Usuarios.ToListAsync());
        }

        // GET: Usuarios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Usuarios = await _context.F5Usuarios
                .FirstOrDefaultAsync(m => m.IdUsuario == id);
            if (f5Usuarios == null)
            {
                return NotFound();
            }

            return View(f5Usuarios);
        }

        // GET: Usuarios/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdUsuario,Ativo,Nome,Senha,Email,Nascimento,Sexo,Foto,Cep,Endereco,Numero,Complemento,Bairro,Cidade,Estado,Atualizacao,DataCadastro")] F5Usuarios f5Usuarios)
        {
            if (ModelState.IsValid)
            {
                _context.Add(f5Usuarios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(f5Usuarios);
        }

        // GET: Usuarios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Usuarios = await _context.F5Usuarios.FindAsync(id);
            if (f5Usuarios == null)
            {
                return NotFound();
            }
            return View(f5Usuarios);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdUsuario,Ativo,Nome,Senha,Email,Nascimento,Sexo,Foto,Cep,Endereco,Numero,Complemento,Bairro,Cidade,Estado,Atualizacao,DataCadastro")] F5Usuarios f5Usuarios)
        {
            if (id != f5Usuarios.IdUsuario)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(f5Usuarios);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!F5UsuariosExists(f5Usuarios.IdUsuario))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(f5Usuarios);
        }

        // GET: Usuarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Usuarios = await _context.F5Usuarios
                .FirstOrDefaultAsync(m => m.IdUsuario == id);
            if (f5Usuarios == null)
            {
                return NotFound();
            }

            return View(f5Usuarios);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var f5Usuarios = await _context.F5Usuarios.FindAsync(id);
            _context.F5Usuarios.Remove(f5Usuarios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool F5UsuariosExists(int id)
        {
            return _context.F5Usuarios.Any(e => e.IdUsuario == id);
        }
    }
}
