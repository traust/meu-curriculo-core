﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MeuCurriculo.Models;
using System.Text;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;

namespace MeuCurriculo.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _configuration = config;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    //    [HttpPost]
    //    //[ValidateAjaxAntiForgeryToken]
    //    public ActionResult Mail(ContatoViewModel contato)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            contato.CriadoEm = DateTime.Now;
    //            //SendEmailContato(contato);
    //        }

    //        return Json(new { });
    //    }

    //    private string GetConfigValue(string key) => _configuration["Communication:" + key];

    //    private bool SendEmailContato(ContatoViewModel contato)
    //    {
    //        try
    //        {
    //            StringBuilder conteudo = new StringBuilder();

    //            conteudo.Append("Contato realizado em " + contato.CriadoEm.ToUniversalTime().ToShortDateString() + " às " + contato.CriadoEm.ToUniversalTime().ToShortTimeString() + "<br><br>");
    //            conteudo.Append("<b>" + "Nome do contato: " + "</b>" + contato.Nome + "<br>");
    //            conteudo.Append("<b>" + "Email: " + "</b>" + contato.Email + "<br>");
    //            conteudo.Append("<br><br><b>Excel friendly:</b>" + contato.CriadoEm.ToUniversalTime().ToString() + ";" + contato.Nome + ";" + contato.Telefone + ";" + contato.Email + ";" + contato.Mensagem);

    //            MailMessage msg = new MailMessage(GetConfigValue("From:User"), GetConfigValue("To:User"));
    //            msg.Subject = "Contato Traust";
    //            msg.SubjectEncoding = Encoding.Default;
    //            msg.Body = conteudo.ToString();
    //            msg.BodyEncoding = Encoding.Default;
    //            msg.IsBodyHtml = true;

    //            SmtpClient oStmp = new SmtpClient();
    //            oStmp.Send(msg);
    //            msg.Dispose();
    //        }
    //        catch (System.Net.Mail.SmtpException)
    //        {
    //            return false;
    //        }
    //        catch (Exception)
    //        {
    //            return false;
    //        }
    //        return true;
    //    }
    }

    //[Serializable]
    //public class ContatoViewModel
    //{
    //    public string Nome { get; set; }

    //    public string Email { get; set; }

    //    public DateTime CriadoEm { get; set; }
    //    public string Telefone { get; internal set; }
    //    public string Mensagem { get; internal set; }
    //}
}
