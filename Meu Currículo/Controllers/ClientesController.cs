﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MeuCurriculo.Data;
using MeuCurriculo.Data.Domain;
using Microsoft.AspNetCore.Authorization;

namespace MeuCurriculo.Controllers
{
    [Authorize]
    public class ClientesController : Controller
    {
        private readonly MeuCurriculoDbContext _context;

        public ClientesController(MeuCurriculoDbContext context)
        {
            _context = context;
        }

        // GET: Clientes
        public async Task<IActionResult> Index(Models.Query query)
        {
            var clientes = await _context.F5Clientes
                .Skip(query.PageSize * query.Page)
                .Take(query.PageSize)
                .ToListAsync();

            query.Total = _context.F5Clientes.Count();
            ViewData["query"] = query;
            return View(clientes);
        }

        public async Task<IActionResult> Emails()
        {
            return View(await _context.F5Clientes
                .Select(s => new Tuple<string, string>(s.Nome, s.Email))
                .ToListAsync());
        }

        // GET: Clientes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Clientes = await _context.F5Clientes
                .FirstOrDefaultAsync(m => m.IdCliente == id);
            if (f5Clientes == null)
            {
                return NotFound();
            }

            return View(f5Clientes);
        }

        // GET: Clientes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCliente,Ativo,Nome,Email,Telefone,Celular,Cpf,Nascimento,Cep,Endereco,Numero,Complemento,Bairro,Cidade,Estado,Senha,Observacoes,DataCadastro,Atualizacao")] F5Clientes f5Clientes)
        {
            if (ModelState.IsValid)
            {
                _context.Add(f5Clientes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(f5Clientes);
        }

        // GET: Clientes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Clientes = await _context.F5Clientes.FindAsync(id);
            if (f5Clientes == null)
            {
                return NotFound();
            }
            return View(f5Clientes);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCliente,Ativo,Nome,Email,Telefone,Celular,Cpf,Nascimento,Cep,Endereco,Numero,Complemento,Bairro,Cidade,Estado,Senha,Observacoes,DataCadastro,Atualizacao")] F5Clientes f5Clientes)
        {
            if (id != f5Clientes.IdCliente)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(f5Clientes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!F5ClientesExists(f5Clientes.IdCliente))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(f5Clientes);
        }

        // GET: Clientes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Clientes = await _context.F5Clientes
                .FirstOrDefaultAsync(m => m.IdCliente == id);
            if (f5Clientes == null)
            {
                return NotFound();
            }

            return View(f5Clientes);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var f5Clientes = await _context.F5Clientes.FindAsync(id);
            _context.F5Clientes.Remove(f5Clientes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool F5ClientesExists(int id)
        {
            return _context.F5Clientes.Any(e => e.IdCliente == id);
        }
    }
}
