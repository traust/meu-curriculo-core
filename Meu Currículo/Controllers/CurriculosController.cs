﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MeuCurriculo.Data;
using MeuCurriculo.Data.Domain;

namespace MeuCurriculo.Controllers
{
    public class CurriculosController : Controller
    {
        private readonly MeuCurriculoDbContext _context;

        public CurriculosController(MeuCurriculoDbContext context)
        {
            _context = context;
        }

        // GET: Curriculos
        public async Task<IActionResult> Index(Models.Query query)
        {
            var curriculos = await _context.F5Curriculos
                .Skip(query.PageSize * query.Page)
                .Take(query.PageSize)
                .ToListAsync();

            query.Total = _context.F5Curriculos.Count();
            ViewData["query"] = query;
            return View(curriculos);
        }

        // GET: Curriculos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Curriculos = await _context.F5Curriculos
                .FirstOrDefaultAsync(m => m.IdCurriculo == id);
            if (f5Curriculos == null)
            {
                return NotFound();
            }

            return View(f5Curriculos);
        }

        // GET: Curriculos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Curriculos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCurriculo,IdCliente,IdPlano,IdLayout,Sexo,Nome,Email,Telefone,Telefone2,Celular,Cpf,FotoDestaque,Nascimento,EstadoCivil,Site,MostrarSite,Cep,Endereco,Bairro,Cidade,Estado,Pais,Perfil,Voluntariado,Intercambios,Interesses,Referencias,PretensaoSalarial,OutrasInformacoes,DataCadastro,DataExpiracao,Valor,Cupom,CupomValor,CupomTipo,Situacao,Expirou,GatewayPagamento,FormaPagamento,ObservacoesInternas,Atualizacao")] F5Curriculos f5Curriculos)
        {
            if (ModelState.IsValid)
            {
                _context.Add(f5Curriculos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(f5Curriculos);
        }

        // GET: Curriculos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Curriculos = await _context.F5Curriculos.FindAsync(id);
            if (f5Curriculos == null)
            {
                return NotFound();
            }
            return View(f5Curriculos);
        }

        // POST: Curriculos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCurriculo,IdCliente,IdPlano,IdLayout,Sexo,Nome,Email,Telefone,Telefone2,Celular,Cpf,FotoDestaque,Nascimento,EstadoCivil,Site,MostrarSite,Cep,Endereco,Bairro,Cidade,Estado,Pais,Perfil,Voluntariado,Intercambios,Interesses,Referencias,PretensaoSalarial,OutrasInformacoes,DataCadastro,DataExpiracao,Valor,Cupom,CupomValor,CupomTipo,Situacao,Expirou,GatewayPagamento,FormaPagamento,ObservacoesInternas,Atualizacao")] F5Curriculos f5Curriculos)
        {
            if (id != f5Curriculos.IdCurriculo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(f5Curriculos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!F5CurriculosExists(f5Curriculos.IdCurriculo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(f5Curriculos);
        }

        // GET: Curriculos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var f5Curriculos = await _context.F5Curriculos
                .FirstOrDefaultAsync(m => m.IdCurriculo == id);
            if (f5Curriculos == null)
            {
                return NotFound();
            }

            return View(f5Curriculos);
        }

        // POST: Curriculos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var f5Curriculos = await _context.F5Curriculos.FindAsync(id);
            _context.F5Curriculos.Remove(f5Curriculos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool F5CurriculosExists(int id)
        {
            return _context.F5Curriculos.Any(e => e.IdCurriculo == id);
        }
    }
}
