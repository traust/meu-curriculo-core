﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using MeuCurriculo.Data;
using MeuCurriculo.Data.Domain;
using MeuCurriculo.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeuCurriculo.Controllers.api
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CurriculosController : ControllerBase
    {
        private readonly MeuCurriculoDbContext _meuCurriculoContext;
        private readonly CurriculoService _curriculoService;

        public CurriculosController(
            MeuCurriculoDbContext context,
            CurriculoService curriculoService)
        {
            _meuCurriculoContext = context;
            _curriculoService = curriculoService;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Mostrar([FromForm]int curriculoId, [FromForm] bool mostrar)
        {
            try
            {
                var curriculo = await _meuCurriculoContext.F5Curriculos.SingleAsync(s => s.IdCurriculo == curriculoId);
                curriculo.MostrarSite = mostrar.ToDatabaseValue();
                await _meuCurriculoContext.SaveChangesAsync();

                return Ok(new { message = "O link" + (!mostrar ? " não" : "") + " será mostrado no seu currículo!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = "Currículo não encontrado" });
            }
        }

        [HttpPost]
        public async Task<ActionResult<int>> Publicar([FromForm]int curriculoId, [FromForm] bool publicar, [FromForm] string curriculoSite = null)
        {
            try
            {
                var curriculo = await _meuCurriculoContext.F5Curriculos.SingleAsync(s => s.IdCurriculo == curriculoId);
                curriculo.Situacao = publicar ? "publicado" : "aprovado";
                if (!publicar)
                {
                    curriculo.MostrarSite = "";
                    curriculo.Site = "";
                }
                else
                    curriculo.Site = curriculoSite;

                await _meuCurriculoContext.SaveChangesAsync();

                return Ok(new { message = (publicar ? "Publicado" : "Despublicado") + " com sucesso!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = "Currículo não encontrado" });
            }
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CheckName([FromForm]string curriculoNome)
        {
            try
            {
                if (string.IsNullOrEmpty(curriculoNome))
                    throw new Exception();

                var existe = await _meuCurriculoContext.F5Curriculos
                    .AnyAsync(c => c.Site.Equals(curriculoNome.ToLowerInvariant()));

                return Ok(existe);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = "Currículo não encontrado" });
            }
        }

        [HttpPost]
        public async Task<ActionResult<bool>> Copiar([FromForm]int originId, [FromForm]int destinyId)
        {
            F5Curriculos origin = null, destiny = null;
            IEnumerable<F5CurriculosCampos> originCampos = null, destinyCampos = null;
            IEnumerable<F5CurriculosOrdenacao> originOrdenacoes = null, destinyOrdenacoes = null;
            IEnumerable<F5CurriculosPersonalizacao> originPersonalizacoes = null, destinyPersonalizacoes = null;
            IEnumerable<F5CurriculosVinculados> originVinculados = null, destinyVinculados = null;
            try
            {
                origin = await _curriculoService.Get(originId);
                destiny = await _curriculoService.Get(destinyId);
                var destinyDataExpiracao = destiny.DataExpiracao;
                var destinySite = destiny.Site;
                var destinyIdCliente = destiny.IdCliente;

                originCampos = await _curriculoService.GetCampos(originId);
                originOrdenacoes = await _curriculoService.GetOrdenacoes(originId);
                originPersonalizacoes = await _curriculoService.GetPersonalizacoes(originId);
                originVinculados = await _curriculoService.GetVinculados(originId);

                destinyCampos = await _curriculoService.GetCampos(destinyId);
                destinyOrdenacoes = await _curriculoService.GetOrdenacoes(destinyId);
                destinyPersonalizacoes = await _curriculoService.GetPersonalizacoes(destinyId);
                destinyVinculados = await _curriculoService.GetVinculados(destinyId);

                destiny = origin.Clone();
                destiny.IdCliente = destinyIdCliente;
                destiny.IdCurriculo = destinyId;
                destiny.DataExpiracao = destinyDataExpiracao;
                destiny.Site = destinySite;

                destinyCampos = Copy(originCampos, destinyId);
                destinyOrdenacoes = Copy(originOrdenacoes, destinyId);
                destinyPersonalizacoes = Copy(originPersonalizacoes, destinyId);
                destinyVinculados = Copy(originVinculados, destinyId);

                //_meuCurriculoContext.Entry<F5Curriculos>(destiny).State = EntityState.Modified;
                _meuCurriculoContext.Update(destiny);

                destinyCampos.ToList().ForEach(e => _meuCurriculoContext.AddOrUpdate(e));
                destinyOrdenacoes.ToList().ForEach(e => _meuCurriculoContext.AddOrUpdate(e));
                destinyPersonalizacoes.ToList().ForEach(e => _meuCurriculoContext.AddOrUpdate(e));
                destinyVinculados.ToList().ForEach(e => _meuCurriculoContext.AddOrUpdate(e));

                await _meuCurriculoContext.SaveChangesAsync();

                return Ok(true);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    message = "Currículos não encontrados",
                    exception = e.Message,
                    innerException = e.InnerException?.Message,
                    origin,
                    destiny
                });
            }
        }

        private List<F5CurriculosVinculados> Copy(IEnumerable<F5CurriculosVinculados> originVinculados, int destinyId)
        {
            List<F5CurriculosVinculados> newEntities = new List<F5CurriculosVinculados>();
            originVinculados.ToList().ForEach(a =>
            {
                var newEntity = a.Clone();
                newEntity.IdVinculado = 0;
                newEntity.IdCurriculoPrimario = destinyId;
                newEntities.Add(newEntity);
            });
            return newEntities;
        }

        private List<F5CurriculosCampos> Copy(IEnumerable<F5CurriculosCampos> originCampos, int destinyId)
        {
            List<F5CurriculosCampos> newEntities = new List<F5CurriculosCampos>();
            originCampos.ToList().ForEach(a =>
            {
                var newEntity = a.Clone();
                newEntity.Id = 0;
                newEntity.IdCurriculo = destinyId;
                newEntities.Add(newEntity);
            });
            return newEntities;
        }

        private List<F5CurriculosOrdenacao> Copy(IEnumerable<F5CurriculosOrdenacao> originCampos, int destinyId)
        {
            List<F5CurriculosOrdenacao> newEntities = new List<F5CurriculosOrdenacao>();
            originCampos.ToList().ForEach(a =>
            {
                var newEntity = a.Clone();
                newEntity.IdOrdenacao = 0;
                newEntity.IdCurriculo = destinyId;
                newEntities.Add(newEntity);
            });
            return newEntities;
        }

        private List<F5CurriculosPersonalizacao> Copy(IEnumerable<F5CurriculosPersonalizacao> originCampos, int destinyId)
        {
            List<F5CurriculosPersonalizacao> newEntities = new List<F5CurriculosPersonalizacao>();
            originCampos.ToList().ForEach(a =>
            {
                var newEntity = a.Clone();
                newEntity.IdPersonalizacao = 0;
                newEntity.IdCurriculo = destinyId;
                newEntities.Add(newEntity);
            });
            return newEntities;
        }
    }
}