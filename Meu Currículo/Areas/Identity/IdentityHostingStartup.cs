﻿using MeuCurriculo.Data;
using MeuCurriculo.Data.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(MeuCurriculo.Areas.Identity.IdentityHostingStartup))]
namespace MeuCurriculo.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
        //public void Configure(IWebHostBuilder builder)
        //{
        //    builder.ConfigureServices((context, services) => {
        //        services.AddDbContext<ApplicationDbContext>(options =>
        //            options.UseSqlServer(
        //                context.Configuration.GetConnectionString("ApplicationDbContextConnection")));

        //        services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = true)
        //            .AddEntityFrameworkStores<ApplicationDbContext>();
        //    });
        //}
    }
}